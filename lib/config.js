var config = {
  options: {
    uploadDir: __dirname+'/../public/uploads/',
    uploadUrl: '/uploads'
  }
}
module.exports = config;