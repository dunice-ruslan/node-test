var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , ObjectId = Schema.ObjectId;

// User Schema
var fileSchema = mongoose.Schema({
  file_name: { type: String, required: true},
  user_id: { type: String, required: true},
  caption: { type: String, required: true, unique: false},
  tags: { type: []}
});

module.exports = mongoose.model('File', fileSchema);
