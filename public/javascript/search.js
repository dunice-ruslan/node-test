$(document).ready(function(){

  $('#search').click(function(){
    $('#form-val').parsley('validate');
    if ($('#form-val').parsley('isValid')) {
        $(".previews tbody").html('')
        $.post('/search',{tags: $('#tags').val(), caption: $('#caption').val()},function(files){
          _.each(files,function(file){
            tags = " "
            _.each(file.tags, function(tag){
                    tags += "<span class='label label-info'>" + tag + "</span> "
            })
            $(".previews tbody").append("<tr id='"+file._id+"'><td><a href='/uploads/"+ file.user_id + '/' + file.file_name +"'>"+file.file_name+"</a></td><td>"+file.caption+"</td><td>"+tags+"</td><td class='remove-file' style='cursor:pointer;'><span class='close'>X</span></td></tr>")
          })
        })
    }
  })
  $(document).on('click','td.remove-file', function(){
    var file_id = $(this).parent().attr('id')
    $.get('/remove_file/'+file_id,function(){
      $('#'+file_id).remove()
    })
  })
})
