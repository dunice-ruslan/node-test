$(document).ready(function(){
  Dropzone.options.localDropzone = {

    autoProcessQueue: false,
    parallelUploads: 100,
    init: function() {
      var submitButton = document.querySelector("#submit-all")
      localDropzone = this;

      submitButton.addEventListener("click", function() {
        $('#val-form').parsley('validate');
        if ($('#val-form').parsley('isValid')) {
            if (localDropzone.files.length<20){
              alert('Files count should min is 20')
            }else{
              _.each(localDropzone.files,function(file){
                $.post('/save_file_info', { file_name:file.name, caption: $("#caption").val(), tags: $("#tags").val()},function(file,status){
                  if (status=='success'){
                  }else{
                    $('.errorContainer').html(status.err)
                    console.log(err)
                  }

                })
              })
              localDropzone.processQueue();
            }
        }
      });

      this.on("addedfile", function() {
        // Show submit button here and/or inform user to click it.
      });

    }
  };
});
