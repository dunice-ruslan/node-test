var ensureAuthenticated = require('../lib/middleware'),
  config = require('../lib/config'),
  _ = require('underscore'),
  fs = require('fs');
var File = require('../models/file.js')
module.exports = function(app){

  app.get("/search", ensureAuthenticated,function (req, res) {
    res.render('search',{user: req.user})
  });

  app.post("/search", function(req,res){
    var res_arr = []
    var caption = req.body.caption;
    if (req.body.tags && req.body.tags!==undefined){
      _.each(req.body.tags.split(','),function(tag){
        res_arr.push({
          tags:{
            $in: [tag.trim()]
          }
        })
      })
    }
    if (caption && caption!==undefined){
      res_arr.push({
        caption: caption
      })
    }

    var q = {
      $and: res_arr
    }
    File.find(q,function(err,files){
      if (!err){
        res.send(files)
      }else{
        res.send([])
      }
    })
  })
}