var ensureAuthenticated = require('../lib/middleware'),
    config = require('../lib/config'),
    _ = require('underscore'),
    fs = require('fs');
var File = require('../models/file.js')
module.exports = function(app){

  app.post("/upload", function (req, res) {
    var userFilesDir = config.options.uploadDir + req.user._id + "/"
    fs.exists(userFilesDir, function (exists) {
      if (!exists){
        fs.mkdir(userFilesDir)
      }
    })
    fs.readFile(req.files.file.path, function (err, data) {
      var newPath = userFilesDir+req.files.file.name;
      fs.writeFile(newPath, data, function (err) {
          res.send(200);
      });
    });
  });

  app.post("/save_file_info", function(req,res){
    var tags = [];
    _.each(req.body.tags.split(','), function(tag){
      tags.push(tag.trim())
    })
    var file = new File({user_id: req.user._id, caption: req.body.caption, tags: tags, file_name: req.body.file_name});
    file.save(function(err) {
      if(err) {
        console.log(err);
        res.send(err);
      } else {
        res.send(file);
      }
    });
  })

  app.get("/files", function(req,res){
    File.find({},function(err, files){
      res.send(files)
    })
  })

  app.get("/upload", ensureAuthenticated, function(req, res){
    res.render("upload",{user: req.user});
  })

  app.get("/remove_file/:id", function(req,res){
    File.findById(req.params.id,function(err,file){
      if (!err){

        fs.unlink(config.options.uploadDir+file.user_id+'/'+file.file_name,function(){
          file.remove();
        })
      }
      res.send(200)
    });
  })
}
