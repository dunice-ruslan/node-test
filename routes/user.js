var ensureAuthenticated = require('../lib/middleware'),
    passport = require('passport'),
    User = require('../models/user.js');

module.exports = function(app){
  app.get('/account', ensureAuthenticated, function(req, res){
    res.render('account', { user: req.user });
  });

  app.get('/login', function(req, res){
    res.render('login', { user: req.user, message: req.session.messages });
  });

  app.post('/login', function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
      if (err) { return next(err) }
      if (!user) {
        req.session.messages =  [info.message];
        return res.redirect('/login')
      }
      req.logIn(user, function(err) {
        if (err) { return next(err); }
        return res.redirect('/');
      });
    })(req, res, next);
  });

  app.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
  });

  app.get('/sign_up', function(req, res){
    res.render('sign_up', { user: req.user, message: req.session.messages });
  });

  app.post('/sign_up', function(req, res) {
    var user = new User({ email: req.body.email, password: req.body.password, username: req.body.username });
    user.save(function(err) {
      if(err) {
        console.log(err);
      } else {
        req.login(user, function(err) {
          if (err) {
            console.log(err)
          }
          return res.redirect('/');
        });
      }
    });
  });

}
